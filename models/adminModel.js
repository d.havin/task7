const admins = [];

module.exports = class Admin {
 
    constructor(adminId, adminName, adminAge){
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminAge = adminAge;
    }
    save(){
        admins.push(this);
    }
    static getAll(){
        return admins;
    }
}