const users = [];
 
module.exports = class User {
 
    constructor(userId, userName, userAge){
        this.userId = userId;
        this.userName = userName;
        this.userAge = userAge;
    }
    save(){
        users.push(this);
    }
    static getAll(){
        return users;
    }
}