const fs =  require('fs');
const express = require("express");
const fileWorkerRouter = express.Router();

const multer = require('multer');
const uploaded = multer().single('file');

const fileWorkerController = require("../controllers/fileWorkerController");

fileWorkerRouter.post("/writeSimple", fileWorkerController.writeFile);
fileWorkerRouter.get("/readSimple", fileWorkerController.readFile)
fileWorkerRouter.get("/writeStream", fileWorkerController.writeableStream);
fileWorkerRouter.get("/readStream", fileWorkerController.readableStream);
fileWorkerRouter.post("/writeFile", uploaded, fileWorkerController.spyStream);

module.exports = fileWorkerRouter;
