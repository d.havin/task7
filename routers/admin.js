const express = require("express");
const adminRouter = express.Router();
const adminParser = express.json();
const adminController = require("../controllers/adminController");

adminRouter.post("/create", adminParser , adminController.adminCreate);
adminRouter.get("/:id", adminParser, adminController.getAdminInfo);
adminRouter.delete("/:id", adminParser, adminController.deleteAdmin);
adminRouter.put("/:id", adminParser, adminController.updateAdmin);

module.exports = adminRouter;

