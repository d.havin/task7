const express = require("express");
const userRouter = express.Router();
const bodyParser = require("body-parser");
const userParser = bodyParser.urlencoded({extended: true});
const userController = require("../controllers/userController");

userRouter.post("/create", userParser, userController.userCreate);
userRouter.get("/:id", userParser, userController.getUserInfo);
userRouter.delete("/:id", userParser, userController.deleteUser);
userRouter.put("/:id", userParser, userController.updateUser);

module.exports = userRouter;
